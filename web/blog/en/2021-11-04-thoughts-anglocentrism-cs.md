# On anglocentrism in computer science

Short premise: what follows are badly elaborated, likely unoriginal thoughts
that I decided to write down after reading
[this toot](https://social.wake.st/@liaizon/107191596894574754) and a gemini
post by nytpu
([imagining a localized programming language](gemini://nytpu.com/gemlog/2021-10-31.gmi)
-- [http mirror](https://portal.mozz.us/gemini/nytpu.com/gemlog/2021-10-31.gmi)).
Also, as it's relevant to the matter at hand, I'm Italian, and I can read and
listen to technical English without issue[^1].

## Programming languages

Easy things first, does it make any sense to internationalize a programming
language?  I'd say these are the issues that could arise from a linguistic
hegemony:

- difficulty in learning syntax and standard libraries;
- control structures limited to what can be naturally expressed in the dominant
  language.

An elegant solution to the first problem is employing purely symbolic names:
see [APL](https://en.wikipedia.org/wiki/APL_\(programming_language\))
and the [λ-calculus](https://en.wikipedia.org/wiki/Lambda_calculus). However I
don't think that memorising an handful of terms is a significant hurdle,
especially if there's a translated manual available (but more on this
later).

The latter point is definitely more intriguing, but I'm still in the "not much
of a problem" camp: even if we assume that natural languages influence the
design of programming structures, as a matter of fact most people will think
in their mother tongue when brainstorming, so they're not impeded at this stage;
after that, bending the rules of English is easy if necessary. As an example,
[if-then-else had to be invented](https://github.com/e-n-f/if-then-else/blob/master/if-then-else.md).

## Documentation

It's probably not controvertial to state that the crux of the matter isn't the
code itself. Language references, academic papers, textbooks and programming
manuals: this is where, in my experience, most people struggle. For the more
popular "static" (i.e. rarely updated) content, translation are sometimes
available (although vary much in quality), while often changing documents are
pretty much a lost cause. Keeping different versions in sync is hard work that
must be carryed on by "reference translators", as a programmer can't be
expected to be able to update documentation written in languages they don't
know; also, people capable of doing this kind of work may consider it a waste
of time and effort.

## The bigger picture

This begs the question: would all this really be worth it? If everyone writes
in the same language, learning that single tongue gives you access to a
collection of information far larger than what would otherwise be possible.  As
an example of this principle in action, I often use the Italian Wikipedia as an
Italian-English dictionary for technical terms: once I find the page I'm
interested in, I immediatly switch to the corresponding English version.

The truth is that I, like many others, am not willing to give up on this.
In the end, linguistic monoculture enables the exchange of knowledge between
parties with just one common trait, having learned the *lingua franca*[^2].

I so wish we ended up with a better international language, be it Esperanto or
whatever, but sadly this isn't the world we live in. Quite disappointing.

<!-- On the other end, I recognize that a problem exists: and it's *English*. Or in
an alternative timeline German, French, Latin: to rely on an ambiguous, messy,
irregular and hard to learn tool for such a vital task is a big error. We have
the know-how[^3] to fix this, but we don't: we (the "cultural subjects") have
accepted the need of learning the imposed foreign language, and the
"conquerors" would never give their favoured position up.

I don't see how the situation could improve in the future: at the cost of being
overly dramatic, I'll let Seneca speak for me:

> *Levis est malitia, saepe mutatur, non in melius sed in aliud*  
> [Evil is unstable, it often changes, not for the better but for something different]

Maybe after the next war we'll end up speaking Chinese, but I really can't
imagine a world where an Italian and a Russian can communicate in 
[Lojban](https://en.wikipedia.org/wiki/Lojban)[^4]. Human perfectibility?
Primacy of Reason? Not on this planet.

When I started writing I didn't plan to end with pessimistic blabber, better
stop here before I ruin someone's day ;) -->


[^1]: I'll let the reader judge how I fare in writing 🙃.

[^2]: Another, less significant advantage is that you can be reasonably certain
  that searching for something in the common language will yield pretty much
  all relevant/interesting results.

<!--
[^3]: Given an 19th-century eye doctor was able to create the most widely
  spoken constructed language, Esperanto, I'd say that we should be able to
  design a perfectly passable conlang with today's tools and the benefit of
  hindsight.

[^4]: Just a random example, I'm not suggesting that Lojban should be our
  universal language -- it wasn't designed for this.
-->
